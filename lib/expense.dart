import 'package:uuid/uuid.dart';

const uuid = Uuid();

enum Category {
  food,
  travel,
  leisure,
  work,
}

/// Expense class container has five variable [id], [title], [amount], [date]
/// type of [String]. Where [title],[amount],[date] required named parameter
/// and [id] is auto generated with the help of [Uuid] of v4.
class Expense {
  Expense({
    required this.title,
    required this.amount,
    required this.date,
    required this.category,
  }) : id = uuid.v4();

  final String id;
  final String title;
  final double amount;
  final DateTime date;
  final Category category;
}
